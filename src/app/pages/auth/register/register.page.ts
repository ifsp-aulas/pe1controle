import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  
  registerForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: LoginService,
  ) { }

  ngOnInit() {
    this.registerForm = this.builder.group({
      email: ['',[Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  createUser(){
    
    const user = this.registerForm.value;
    
    console.log(user);
    this.service.createUser(user);
  }
}
