import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {

  forgotForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: LoginService,
  ) { }

  ngOnInit() {
    this.forgotForm = this.builder.group({
      email: ['',[Validators.required, Validators.email]],
    });
  }

  recoverPass(){
    const data = this.forgotForm.value;
    this.service.recoverPass(data);
  }

}
