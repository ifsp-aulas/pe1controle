import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ContaService } from '../service/conta-service.service';

@Component({
  selector: 'pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  listaContas;
  tipo;

  constructor(
    private conta: ContaService
  ) { }

  ngOnInit() {
    this.listarContas();
  }

  listarContas(){
    this.conta.lista(this.tipo).
    subscribe(x => this.listaContas = x);
  }

  remove(conta){
    this.conta.remove(conta);
  }
}
