// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCIbR4oK5ev20mraY1QB2FcJOi3-V-iv14',
    authDomain: 'controle-ifsp.firebaseapp.com',
    databaseURL: 'https://controle-ifsp.firebaseio.com',
    projectId: 'controle-ifsp',
    storageBucket: 'controle-ifsp.appspot.com',
    messagingSenderId: '659104997584',
    appId: '1:659104997584:web:a101dbeb5c744915b67ed5',
    measurementId: 'G-HLBNL5TR82'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
